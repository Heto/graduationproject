﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Dialog : MonoBehaviour {

    public Text textDisplay;
    public string[] sentences;
    private int index;
    public float typingSpeed;
    public PlayerController player;
    private int numOfIndex;

    public GameObject continueButton;

    void Awake()
    {
        player = FindObjectOfType<PlayerController>();
        numOfIndex = 0;
    }

    void Start()
    {        
        StartCoroutine(Type());
    }

    void Update()
    {
        if (textDisplay.text == sentences[index])
        {
            continueButton.SetActive(true);
        }

        if (numOfIndex == sentences.Length)
            Destroy(this.gameObject);
    }

	IEnumerator Type()
    {
        foreach(char letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
    }

    public void NextSentence()
    {
        continueButton.SetActive(false);
        numOfIndex++;
        Debug.Log(numOfIndex);
        if (index < sentences.Length - 1)
        {
            index++;
            textDisplay.text = "";
            StartCoroutine(Type());
        }
        else
        {
            textDisplay.text = "";
            continueButton.SetActive(false);     
        }
    }

    void OnEnable()
    {
        player.onDialogisPlaying = true;
    }

    void OnDestroy()
    {
        player.onDialogisPlaying = false;
    }
}
