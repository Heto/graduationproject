﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButtonClick : MonoBehaviour {

    public float alpha = 0.1f;
	// Use this for initialization
	void Start () {
        GetComponent<Image>().alphaHitTestMinimumThreshold = alpha;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
}
