﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CutSceneManager : MonoBehaviour {

    public GameObject[] cutscene;
	// Use this for initialization
	void Start () {
        cutscene[0].SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space))
        {
            if (cutscene[0].activeSelf)
            {
                cutscene[0].SetActive(false);
                cutscene[1].SetActive(true);
            }
            else if (cutscene[1].activeSelf)
            {
                cutscene[1].SetActive(false);
                cutscene[2].SetActive(true);
            }
            else if (cutscene[2].activeSelf)
            {
                cutscene[2].SetActive(false);
                cutscene[3].SetActive(true);
            }
            else if (cutscene[3].activeSelf)
            {
                cutscene[3].SetActive(false);
                cutscene[4].SetActive(true);
            }
            else if (cutscene[4].activeSelf)
            {
                cutscene[4].SetActive(false);
                cutscene[5].SetActive(true);
            }
            else if (cutscene[5].activeSelf)
            {
                SceneManager.LoadScene(2);
            }
        }
	}

}
