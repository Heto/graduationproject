﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour {

    public Material doorClose;
    public Material doorOpen;
    public GameObject door;
    public GameObject target;
    public bool isOpen = false;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (isOpen)
        {
            Debug.Log("!231");
            door.transform.position = Vector3.MoveTowards(transform.position, target.transform.position, 5.0f * Time.deltaTime);
        }
            
	}
}
