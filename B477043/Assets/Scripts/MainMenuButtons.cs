﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour {

    public DataManager dm;
    public GameObject optionMenu;
    public GameObject mainMenu;
    public GameObject creditMenu;
    public GameObject creditMenuContents;
    private Vector3 contentsStartPosition;

	// Use this for initialization
    void Awake ()
    {
        contentsStartPosition = creditMenuContents.transform.position;
    }

	void Start () {
        dm = FindObjectOfType<DataManager>();
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void ClearData()
    {
        PlayerPrefs.DeleteKey("SavePoint");
        Debug.Log("Data all deleted.");
    }

    public void VolumeSlider(float volume)
    {
        AudioListener.volume = volume;
    }

    public void TutorialCheck(bool checkBox)
    {
        if (checkBox)
        {
            PlayerPrefs.SetInt("Tutorial", 1);
            Debug.Log(PlayerPrefs.GetInt("Tutorial"));
        }
        else
        {
            PlayerPrefs.SetInt("Tutorial", 0);
            Debug.Log(PlayerPrefs.GetInt("Tutorial"));
        }
    }

    public void StartButton()
    {
        dm.savePoint = 0;
        SceneManager.LoadScene(1);
        Debug.Log("New Game");
    }

    public void LoadButton()
    {
        if (PlayerPrefs.HasKey("SavePoint"))
        {
            //PlayerPrefs.Save();

            if (PlayerPrefs.GetInt("SavePoint") == 1)
            {
                //dm.savePoint = 1;
                SceneManager.LoadScene(2);
                Debug.Log("Load Game");
            }
        }
        else
        {
            Debug.Log("There is no save data.");
        }  
    }

    public void OptionButton()
    {
        optionMenu.SetActive(true);
        mainMenu.SetActive(false);
    }

    public void OptionBackButton()
    {
        optionMenu.SetActive(false);
        mainMenu.SetActive(true);
    }

    public void CreditButton()
    {
        creditMenu.SetActive(true);
        mainMenu.SetActive(false);
    }

    public void CreditBackButton()
    {
        creditMenuContents.transform.position = contentsStartPosition;
        creditMenu.SetActive(false);
        mainMenu.SetActive(true);
    }
}
