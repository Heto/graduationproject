﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveDialog : MonoBehaviour {

    public int numberOfDialog;

    public GameObject[] dialog;

    public bool onCraftTutorial = false;
    public bool canCraftTutorial = false;
    public bool onPileTutorial = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (numberOfDialog == 1)
            {
                if (PlayerPrefs.GetInt("Tutorial") == 1 && dialog[numberOfDialog - 1])
                {
                    dialog[numberOfDialog - 1].SetActive(true);
                    onCraftTutorial = true;
                }
                PlayerPrefs.SetInt("SavePoint", 1);
                Debug.Log(PlayerPrefs.GetInt("SavePoint"));
            }

            if (numberOfDialog == 2)
            {
                if (PlayerPrefs.GetInt("Tutorial") == 1 && dialog[numberOfDialog - 1])
                {
                    dialog[numberOfDialog - 1].SetActive(true);
                    onPileTutorial = true;
                }                
            }
        }
    }
}
