﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinueButton : MonoBehaviour {

    public Dialog[] dialog;
    public ActiveDialog[] activeDialog;
    public PlayerController player;
	// Use this for initialization
	void Start () {
        player = FindObjectOfType<PlayerController>();
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space))
        {
            dialog[0].NextSentence();

            if (activeDialog[0].numberOfDialog == 1 && activeDialog[0].onCraftTutorial)
                dialog[1].NextSentence();

            if (activeDialog[0].onCraftTutorial && activeDialog[0].canCraftTutorial)
                dialog[2].NextSentence();

            if (player.madeWoodPlankOnce)
                dialog[3].NextSentence();

            if (activeDialog[1].onPileTutorial)
                dialog[4].NextSentence();
        }
	}

}
