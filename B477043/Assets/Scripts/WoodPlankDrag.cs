﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodPlankDrag : MonoBehaviour {

    float distance = 160;
    public PlayerController playerController;
    public GameObject woodPlank;
    public GameObject crate;
    public GameObject bigStone;
    public int typeOfObject;
    public bool isStick = false;
    public Transform target;
	//void OnMouseDrag()
 //   {
 //       Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
 //       Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);

 //       transform.position = objPosition;
 //   }

    void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
        target = GameObject.FindGameObjectWithTag("Pile").transform;
        playerController.isMaking = true;
    }

    void Update()
    {
        if (isStick == false)
        {
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
            Vector3 rot = new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z);
            transform.rotation = Quaternion.Euler(rot);
            transform.position = objPosition;

            if (Input.GetAxis("Mouse ScrollWheel") < 0)
                distance -= 5;
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
                distance += 5;
        }

        float toPileDistance = (Input.mousePosition - Camera.main.WorldToScreenPoint(target.position)).magnitude;

        if (isStick && toPileDistance > 150f)
            isStick = false;

    }

    void OnMouseDown()
    {
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
        Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
        if (typeOfObject == 1)
        {
            Instantiate(woodPlank, transform.position, transform.rotation);
            playerController.isMaking = false;
        }            
        else if (typeOfObject == 2)
        {
            Instantiate(crate, transform.position, transform.rotation);
            playerController.isMaking = false;
        }
        else if (typeOfObject == 3)
        {
            Instantiate(bigStone, transform.position, transform.rotation);
            playerController.isMaking = false;
        }
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider col)
    {
        if (typeOfObject == 2)
        {
            if (col.gameObject.tag == "Pile")
            {
                isStick = true;
                transform.position = col.transform.position;
            }
        }
    }
}
