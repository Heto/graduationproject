﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PileScript : MonoBehaviour {
    
    private WoodPlankDrag woodPlankDrag;
    public PlayerController playerController;
	// Use this for initialization
	void Start () {
        playerController = FindObjectOfType<PlayerController>();
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    void OnMouseOver()
    {
        //if (woodPlankDrag)
            woodPlankDrag = FindObjectOfType<WoodPlankDrag>();
    }
    void OnMouseExit()
    {
        if (playerController.isMaking)
            woodPlankDrag.isStick = false;
    }
}
